// requiring dependencies
var express = require('express');
var app = express();
var bodyParser = require("body-parser");
var mongoose = require("mongoose");
var flash = require("connect-flash"); 
var server = app.listen(8080);
const io = require('socket.io').listen(server);

var passport = require("passport");
var methodOverride = require("method-override");
var LocalStrategy = require("passport-local")
var User = require('./models/user')
var authRoutes = require('./routes/auth');
var dashboardRoutes = require('./routes/dashboard');

//database connection
const uri = "mongodb+srv://dbUser:db12345@cluster0-vogn1.mongodb.net/test?retryWrites=true&w=majority"
mongoose.connect(uri,{ useNewUrlParser: true });

// routes
app.use(bodyParser.urlencoded({extended: true}));
app.set("view engine","ejs"); //setting view engine
app.use(methodOverride("_method"));
app.use(flash());

// passport configuration // 

app.use(require("express-session")({
    secret: "Hi there",
    resave: false,
    saveUninitialized: false
}));

app.use(passport.initialize());
app.use(passport.session());
passport.use(new LocalStrategy(User.authenticate()));
passport.serializeUser(User.serializeUser());
passport.deserializeUser(User.deserializeUser());

app.use(function(req,res,next){
    if(req.user){
        var userInfo = req.user;
        console.log(userInfo.username)
        res.locals.currentUser = userInfo.username;
    }
    else
    {
        res.locals.currentUser = req.user;
    }
    res.locals.error = req.flash("error");
    res.locals.success = req.flash("success");
    next();
});

app.use(authRoutes);
app.use(dashboardRoutes);

io.sockets.on('connection', function(socket) {
    socket.on('username', function(username) {
        socket.username = username;
        io.emit('is_online', '🔵 <i>' + socket.username + ' join the chat..</i> <br>');
    });

    socket.on('disconnect', function(username) {
        io.emit('is_online', '🔴 <i>' + socket.username + ' left the chat..</i> <br> ');
    })

    socket.on('chat_message', function(message) {
        io.emit('chat_message', '<strong>' + socket.username + '</strong>: ' + message);
    });

});

var port = 1111
app.listen(port,()=>{
    console.log(`server is running at port ${port}`);
})
