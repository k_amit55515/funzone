var middlewareObj = {};

middlewareObj.isLoggedIn = function(req,res,next){
    if (req.isAuthenticated()){
        return next();
    }
    req.flash("error"," Login required !");
    res.redirect("/login");
}

module.exports = middlewareObj;