var express = require("express");
var Router = express.Router();
var passport = require("passport");
var User = require("../models/user");
const user = require("../models/user");

Router.get("/register",function(req,res){
    res.render("register");
});

Router.post("/register",function(req,res){
    var newUser = new User({username: req.body.username});
    User.register(newUser,req.body.password , function(err,user){
        if (err){
            req.flash("error",err.message);
            return res.render("register");
        }
        passport.authenticate("local")(req,res,function(){
            req.flash("success","Welcome "+user.username);
            res.redirect("/dashboard");
        });
    });
});

// show login form
Router.get("/login",function(req,res){
    res.render("login");
});
// handling login logic
Router.post("/login", passport.authenticate("local",
    {
        successRedirect: "/dashboard",
        failureRedirect: "/login"
    }),function(req,res){
});
// logout route
Router.get("/logout", function(req,res){
    req.logout();
    req.flash("success","Logged You Out");
    res.redirect("/");
});

module.exports = Router;