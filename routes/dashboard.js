var express = require("express");
var Router = express.Router();
var user = require("../models/user");
var middleware = require('../middleware');


Router.get('/',(req,res)=>{  // home route
    res.render("landing");
})

Router.get('/dashboard',middleware.isLoggedIn,(req,res)=>{ 
    res.render("dashboard");
})

Router.post("/lobby1",middleware.isLoggedIn,(req,res)=>{
    res.render("lobby1");
})
Router.post("/lobby2",middleware.isLoggedIn,(req,res)=>{
    res.render("lobby2");
})
Router.post("/lobby3",middleware.isLoggedIn,(req,res)=>{
    res.render("lobby3");
})
Router.post("/lobby4",middleware.isLoggedIn,(req,res)=>{
    res.render("lobby4");
})

module.exports = Router;